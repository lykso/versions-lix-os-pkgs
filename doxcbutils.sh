#!/bin/sh -e

echo "xcb-util
xcb-util-image
xcb-util-keysyms
xcb-util-renderutil
xcb-util-wm" | while read pkg; do
	echo $pkg
	mkdir -p /lix-os/versions/pkg/$pkg
	echo "https://xcb.freedesktop.org/dist	<table	<\\/table	>$pkg-([\\d\\.]+)\\.tar\\.bz2<" > /lix-os/versions/pkg/$pkg/url
        versions $pkg
        version="$(versions $pkg | tail -n1)"
	SRCROOT=/lix-os/src /lix-os/utilities/src-new-tarball.sh $pkg $version tar.bz2 "https://xcb.freedesktop.org/dist/$pkg-\$version.\$type"
	mkdir -p /lix-os/how/pkg/$pkg/default
	touch /lix-os/how/pkg/$pkg/default/todo
done
